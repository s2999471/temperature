package nl.utwente.di.temperature;

public class Quoter {
    double getFahrenheit(String temperature) {
        return 32 + 1.8 * Double.parseDouble(temperature);
    }
}
