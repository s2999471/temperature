package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the Quoter.
 */
public class TestQuoter {

    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double temp = quoter.getFahrenheit("1");
        Assertions.assertEquals(33.8, temp, 0.1, "1C to F");
    }
}
